# Google Codelab: Firebase Web Codelab
This is an implementation of a Google codelab tutorial, the tutorial is freely avaliable at: https://codelabs.developers.google.com/codelabs/firebase-web/index.html, this tutorial teaches you to create a simple firebase web app of a live chat, this code is the final product of said tutorial

## Pre-requisites
To test this project you'll need
- [git](https://git-scm.com/)
- [firebase-tool](https://github.com/firebase/firebase-tools)

## Installing

First you'll need to create a new firebase project, this can be via the [firebase console](https://console.firebase.google.com/), after creating it press the **Add Firebase to your web app** button and copy the part that reads
```javascript
var config = {
    ...
};
```
You'll also have to enable google auth, it also can be done at the [firebase console](https://console.firebase.google.com/project/_/authentication/providers)

then run the following on a terminal:
```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Firebase_Web_Codelab.git
cd Firebase_Web_Codelab/src
firebase login
firebase use --add
firebase database:set / initial_messages.json
firebase deploy
```

## Running
You can start the app by running the following command on a terminal:
```
firebase open hosting:site
```
Or check out my deployed version [here](https://friendlychat-f6db1.firebaseapp.com/) 
## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source by [Firebase](https://github.com/firebase) available at: https://github.com/firebase/friendlychat
- Tutorial by [Google Codelabs](https://codelabs.developers.google.com/) available at: https://codelabs.developers.google.com/codelabs/firebase-web/index.html